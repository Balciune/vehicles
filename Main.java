import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    List<Vehicle> vehicles = new ArrayList<>();
    List<Car> cars = new ArrayList<>();
    List<Motorcycle> motorcycles = new ArrayList<>();
    List<Tractor> tractors = new ArrayList<>();

    public static void main(String[] args) {
        File file = new File("VehicleList.txt");
       try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = bufferedReader.readLine()) != null)
                System.out.println(line);
        }
        catch (IOException e) {
            e.printStackTrace();

        }
    }
}
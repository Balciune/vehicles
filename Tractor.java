
public class Tractor extends Vehicle {
    private int maxPulledWeight;
    public Tractor(String brand, String model, int price, int maxPulledWeight) {
        super(brand, model, price);
        this.maxPulledWeight= maxPulledWeight;
    }
    public int getMaxPulledWeight() {
        return maxPulledWeight;
    }
    public void setMaxPulledWeight(int maxPulledWeight) {
        this.maxPulledWeight = maxPulledWeight;
    }
    @Override
    public String toString() {
        return "Tractors{" + super.toString() + "  " + "Max pull weight " + maxPulledWeight +'}';
    }
}
package JavaAdvExercises.Vehicle;

public class Car extends Vehicle {
    private double topSpeed;
    private String transmission;
    private String shape;
    public Car(String brand, String model, int price, double topSpeed, String transmission, String shape) {
        super(brand, model, price);
        this.topSpeed= topSpeed;
        this.transmission= transmission;
        this.shape= shape;
    }
    public double getTopSpeed() {
        return this.topSpeed;
    }
    public void setTopSpeed(double topSpeed) {
        this.topSpeed = topSpeed;
    }
    public String getTransmission() {
        return this.transmission;
    }
    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }
    public String getShape() {
        return this.shape;
    }
    public void setShape(String shape) {
        this.shape = shape;
    }
    public enum CarShape {
        COUPE, SEDAN, WAGON
    }
    public enum Transmision {
        MANUAL, AUTOMATIC
    }
    @Override
    public String toString() {
        return "Car{" + super.toString() + "  " + "Top Spped " + topSpeed + "  " + "Transmission " + transmission + "  " + "Shape" + shape +'}';
    }

}
package JavaAdvExercises.Vehicle;

public class Motorcycle extends Vehicle {
    private double topSpeed;
    private String shape;
    public Motorcycle(String brand, String model, int price, double topSpeed, String shape) {
        super(brand, model, price);
        this.topSpeed= topSpeed;
        this.shape= shape;
    }
    public double getTopSpeed() {
        return this.topSpeed;
    }
    public void setTopSpeed(double topSpeed) {
        this.topSpeed = topSpeed;
    }
    public String getShape() {
        return this.shape;
    }
    public void setShape(String shape) {
        this.shape = shape;
    }
    public enum MotorcycleShape {
        CHOPPER, CRUISER, ENDURO
    }
    @Override
    public String toString() {
        return "Motorcycles{" + super.toString() + "  " + "Top Spped " + topSpeed + "Shape" + shape +'}';
    }

}
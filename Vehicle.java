public class Vehicle  {
    private String brand;
    private String model;
    private int price;

    public Vehicle (String brand, String model, int price){
        this.brand= brand;
        this.model= model;
        this.price= price;
    }
    public String getBrand() {
        return this.brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public String getModel() {
        return this.model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public int getPrice() {
        return this.price;
    }
    public void setPrice(int price) {
        this.price= price;
    }
    @Override
    public String toString(){
        return "Vehicle{" + "Brand = " + brand + "  " +"Model = " + model + "  " + "Price = " + price + "  " + '}';
    }
}
